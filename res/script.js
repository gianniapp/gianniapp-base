'use strict';
var unlockLevel = {
  shop: 10,
  characters: [
    /* id0 */ 0,
    /* id1 */ 20,
    /* id2 */ 30
  ]
}

var defaults = {
  packages: [
    'gianniapp'
  ],
  fruit: 'gianniapp/apple',
  character: 'gianniapp/gianni'
}

var shopCategories = [
  'fruits'
];

var current = {
  fruit: null,
  itemState: 0,
  character: null
}

var packagePreload = {
  count: 0,
  total: 0,
  add: function(location, name) {
    packagePreload.total++;
    PackageManager.load(location, name, false, function(err) {
      if (err) {
        middler.dialog(_('error.title'), err.toString(), 'OK', function() {
          middler.quit();
        });
        throw err;
      }
      packagePreload.count++;
      if (packagePreload.total == packagePreload.count) {
        window.dispatchEvent(new Event('packages-load'));
      }
    })
  }
}

var _, storage, lang, autorefresh, timerTimeout;

function getLevel(syncableData, unsyncableData) {
  if (typeof syncableData != 'object') syncableData = getUserData(true);
  if (typeof unsyncableData != 'object') unsyncableData = getUserData(false);
  if (getUserData(false).vanilla) {
    var fruits = syncableData.vanillaFruits;
  } else {
    var defaultFruitId = PackageManager.id.parse(defaults.fruit);
    var fruits = getFruit(defaultFruitId.packageId, defaultFruitId.itemId, syncableData);
  }
  if (fruits < 100) {
    return Math.floor(fruits / 20);
  } else if (fruits < 600) {
    return Math.floor((fruits - 100) / 50) + 5;
  } else if (fruits < 1000) {
    return Math.floor((fruits - 600) / 80) + 15;
  } else if (fruits < 6000) {
    return Math.floor((fruits - 1000) / 500) + 20;
  } else {
    return Math.floor((fruits - 6000) / 1000) + 30;
  }
}

function getFruitsToNextLevel(syncableData, unsyncableData) {
  if (typeof syncableData != 'object') syncableData = getUserData(true);
  if (typeof unsyncableData != 'object') unsyncableData = getUserData(false);
  if (unsyncableData.vanilla) {
    var fruits = syncableData.vanillaFruits;
  } else {
    var defaultFruitId = PackageManager.id.parse(defaults.fruit);
    var fruits = getFruit(defaultFruitId.packageId, defaultFruitId.itemId, syncableData);
  }
  if (fruits < 100) {
    return 20 - (fruits % 20);
  } else if (fruits < 600) {
    return 50 - ((fruits - 100) % 50);
  } else if (fruits < 1000) {
    return 80 - ((fruits - 600) % 80);
  } else if (fruits < 6000) {
    return 500 - ((fruits - 1000) % 500);
  } else {
    return 1000 - ((fruits - 6000) % 1000);
  }
}

function getFruitsBetweenLevels(syncableData, unsyncableData) {
  if (typeof syncableData != 'object') syncableData = getUserData(true);
  if (typeof unsyncableData != 'object') unsyncableData = getUserData(false);
  if (unsyncableData.vanilla) {
    var fruits = syncableData.vanillaFruits;
  } else {
    var defaultFruitId = PackageManager.id.parse(defaults.fruit);
    var fruits = getFruit(defaultFruitId.packageId, defaultFruitId.itemId, syncableData);
  }
  if (fruits < 100) {
    return 20;
  } else if (fruits < 600) {
    return 50;
  } else if (fruits < 1000) {
    return 80;
  } else if (fruits < 6000) {
    return 500;
  } else {
    return 1000;
  }
}

function ensureInt(value) {
  var parsed = parseInt(value);
  if (isNaN(parsed)) return 0;
  return parsed;
}

function getCoins(data) {
  if (typeof data != 'object') data = getUserData(true);
  return ensureInt(data.coins);
}

function addCoins(amount) {
  amount = ensureInt(amount);
  var data = getUserData(true);
  data.coins = ensureInt(data.coins);
  data.coins += amount;
  setUserData(data, true);
}

function setCoins(amount) {
  amount = ensureInt(amount);
  var data = getUserData(true);
  data.coins = amount;
  setUserData(data, true);
}

function getFruit(packageId, fruitId, data) {
  if (typeof data != 'object') data = getUserData(true);
  if (data.fruitCounts[packageId]) {
    var amount = data.fruitCounts[packageId][fruitId];
    if (!amount) {
      return 0;
    }
    return ensureInt(amount);
  } else if (packages[packageId]) {
    return 0;
  } else {
    throw 'package not loaded';
  }
}

function setFruit(packageId, fruitId, amount) {
  amount = ensureInt(amount);
  var data = getUserData(true);
  if (data.fruitCounts[packageId]) {
    data.fruitCounts[packageId][fruitId] = amount;
  } else if (packages[packageId]) {
    data.fruitCounts[packageId] = {};
    data.fruitCounts[packageId][fruitId] = amount;
  } else {
    throw 'package not loaded';
  }
  setUserData(data, true);
}

function addFruit(packageId, fruitId, amount) {
  amount = ensureInt(amount);
  var data = getUserData(true);
  if (data.fruitCounts[packageId]) {
    if (Number.isInteger(data.fruitCounts[packageId][fruitId])) {
      data.fruitCounts[packageId][fruitId] = ensureInt(data.fruitCounts[packageId][fruitId]);
      data.fruitCounts[packageId][fruitId] += amount;
    } else {
      data.fruitCounts[packageId][fruitId] = amount;
    }
  } else if (packages[packageId]) {
    data.fruitCounts[packageId] = {};
    data.fruitCounts[packageId][fruitId] = amount;
  } else {
    throw 'package not loaded';
  }
  setUserData(data, true);
}

function changeCharacter(packageId, characterId) {
  if (getUserData(false).vanilla) {
    throw "Cannot change character in vanilla mode";
  }
  if (current.character.id != PackageManager.id.strignify(packageId, characterId)) {
    clearTimeout(timerTimeout);
  }
  if (!packages[packageId]) {
    throw 'package not loaded';
  } else if (!packages[packageId].characters) {
    throw 'package does not contain characters';
  } else if (!packages[packageId].characters[characterId]) {
    throw 'package does not contain this character';
  } else {
    var character = packages[packageId].characters[characterId];
    if (getLevel() < character.level) {
      middler.dialog(_('level.notEnough.title'), _('level.notEnough.text', {
        level: character.level,
      }), _('ok'), function() {})
      return;
    }
    character.id = PackageManager.id.strignify(packageId, characterId);
    current.character = character;
    if (current.character.item) {
      current.itemState = 'idle';
    } else {
      current.itemState = 0;
    }
    refresh();
    $('#freeDiv').slideUp();
    $('#main').slideDown();
  }
}

function changeFruit(packageId, fruitId) {
  if (getUserData(false).vanilla) {
    throw "Cannot change fruit in vanilla mode";
  }
  if (current.character.item) {
    throw 'cannot switch fruit if current character has an item';
  }
  if (!packages[packageId]) {
    throw 'package not loaded';
  } else if (!packages[packageId].fruits) {
    throw 'package does not contain fruits';
  } else if (!packages[packageId].fruits[fruitId]) {
    throw 'package does not contain this fruit';
  }
  var fruit = packages[packageId].fruits[fruitId];
  fruit.id = PackageManager.id.strignify(packageId, fruitId);
  if (fruit.id != defaults.fruit) {
    var data = getUserData(true);
    if (!data.purchases.fruits[packageId] || !data.purchases.fruits[packageId][fruitId]) {
      middler.dialog(_('error.title'), _('error.fruitNotBought'), _('ok'), function() {})
      return;
    }
  }
  current.fruit = fruit;
  refresh();
  $('#freeDiv').slideUp();
  $('#main').slideDown();
}

function changeSrcIfNotEqual(elementStr, newSrc) {
  if ($(elementStr).attr('src') != newSrc) {
    $(elementStr).attr('src', newSrc);
  }
}

function refresh() {
  var defaultFruitId = PackageManager.id.parse(defaults.fruit);
  var syncData = getUserData(true);
  var nonSyncData = getUserData(false);
  $('#coinNum').html(getCoins());
  $('#character').attr('title', findTranslatedText(current.character.name));
  $('#character').attr('alt', findTranslatedText(current.character.name));
  changeSrcIfNotEqual('#character', current.character.res.character.src);
  if (current.character.item) {
    $('#topFruitData').removeClass('pointer');
    if (current.itemState == 'using') {
      changeSrcIfNotEqual('#fruit', current.character.res.itemUsing.src);
    } else if (current.itemState == 'done') {
      changeSrcIfNotEqual('#fruit', current.character.res.itemDone.src);
    } else { // item idle
      changeSrcIfNotEqual('#fruit', current.character.res.item.src);
    }
    var fruitId = {};
    for (var i in current.character.item.reward) {
      if (i != 'coin') {
        var fruitId = PackageManager.id.parse(i);
        if (!packages[fruitId.packageId]) {
          fruitId = defaultFruitId;
        }
        break;
      }
    }
    var fruits = getFruit(fruitId.packageId, fruitId.itemId);
    $('#fruitNum').text(fruits);
    changeSrcIfNotEqual('#topFruit', packages[fruitId.packageId].fruits[fruitId.itemId].res[0].src);
    var fruitCountText = _('fruits.count', {
      count: fruits,
      singular: findTranslatedText(packages[fruitId.packageId].fruits[fruitId.itemId].amount.singular),
      plural: findTranslatedText(packages[fruitId.packageId].fruits[fruitId.itemId].amount.plural)
    });
    $('#fruitNum').attr('title', fruitCountText);
    $('#topFruit').attr('title', fruitCountText);
    $('#topFruit').attr('alt', fruitCountText);
    $('#fruit').attr('title', findTranslatedText(current.character.item.name));
    $('#fruit').attr('alt', findTranslatedText(current.character.item.name));
  } else {
    var fruitId = PackageManager.id.parse(current.fruit.id);
    if (nonSyncData.vanilla) {
      var fruits = getUserData(true).vanillaFruits;
    } else {
      var fruits = getFruit(fruitId.packageId, fruitId.itemId);
    }
    var fruitCountText = _('fruits.count', {
      count: fruits,
      singular: findTranslatedText(packages[fruitId.packageId].fruits[fruitId.itemId].amount.singular),
      plural: findTranslatedText(packages[fruitId.packageId].fruits[fruitId.itemId].amount.plural)
    });
    $('#fruitNum').text(fruits);
    $('#topFruitData').addClass('pointer');
    changeSrcIfNotEqual('#fruit', current.fruit.res[current.itemState].src);
    changeSrcIfNotEqual('#topFruit', current.fruit.res[0].src);
    $('#fruitNum').attr('title', fruitCountText);
    $('#topFruit').attr('title', fruitCountText);
    $('#topFruit').attr('alt', fruitCountText);
    $('#fruit').attr('title', findTranslatedText(current.fruit.name));
    $('#fruit').attr('alt', findTranslatedText(current.fruit.name));
  }
  if (nonSyncData.dark) {
    $('body').addClass('dark');
    $('body').removeClass('light');
    $('dialog').addClass('dark');
    $('dialog').removeClass('light');
    $('#topBar').addClass('top-dark');
    $('#topBar').removeClass('top-light');
  } else {
    $('body').removeClass('dark');
    $('body').addClass('light');
    $('dialog').removeClass('dark');
    $('dialog').addClass('light');
    $('#topBar').removeClass('top-dark');
    $('#topBar').addClass('top-light');
  }
  for (var packageId in packages) {
    if (typeof packages[packageId].achievements == 'object') {
      if (!syncData.achievements) {
        syncData.achievements = {};
      }
      if (!syncData.achievements[packageId]) {
        syncData.achievements[packageId] = {};
      }
      for (var achievementId in packages[packageId].achievements) {
        var achievement = packages[packageId].achievements[achievementId];
        if (!syncData.achievements[packageId][achievementId]) {
          syncData.achievements[packageId][achievementId] = checkAchievement(achievement, syncData);
          if (syncData.achievements[packageId][achievementId]) {
            toast(_('achievements.unlock', { name: findTranslatedText(achievement.name) }));
            window.dispatchEvent(new CustomEvent('achievement-unlock', {
              detail: {
                id: {
                  packageId,
                  itemId: achievementId
                }
              }
            }))
          }
        }
      }
    }
  }
  setUserData(syncData, true);
  $(document).localize({
    level: getLevel(),
    version: version.main,
    middlerVersion: version.middler,
    middler: middlerName
  });
}

function toast(text) {
  var doNext = (toast.list.length == 0 && !$('#toast').is(':visible'));
  toast.list.push(text);
  if (doNext) {
    toast.next();
  }
}

toast.list = [];

toast.nextTimeout = null;

toast.next = function() {
  if (toast.nextTimeout) {
    clearTimeout(toast.nextTimeout);
    toast.nextTimeout = null;
  }
  $('#toast').slideUp(undefined, function() {
    if (toast.list.length == 0) return;
    $('#toastText').text(toast.list.shift());
    $('#toast').slideDown(undefined, function() {
      toast.nextTimeout = setTimeout(toast.next, 5000);
    });
  });
}

function popTranslations() {
  var translationText = '';
  for (var i in knownLangs) {
    var code = knownLangs[i];
    if (translationText == "") {
      translationText = i18next.getFixedT(code)('language.name') + ': ' + i18next.getFixedT(code)('language.translator');
    } else {
      translationText += '\n' + i18next.getFixedT(code)('language.name') + ': ' + i18next.getFixedT(code)('language.translator');
    }
  }
  middler.dialog(_('settings.about.translations'), translationText, _('close'));
}

function popPrivacy() {
  middler.dialogMultiple(_('settings.about.privacy.title'), _('settings.about.privacy.text'), [
    _('close'),
    _('settings.about.privacy.gitlab')
  ], function(i) {
    if (i == 1) {
      middler.url('https://gitlab.com/gianniapp');
    }
  });
}

function getUserData(syncable) {
  var defaultData = {}
  if (syncable) {
    var datatext = middler.data.get('syncable');
    defaultData = {
      fruitCounts: {},
      coins: 0,
      vanillaFruits: 0,
      purchases: {
        fruits: {},
      }
    };
    var defaultFruitId = PackageManager.id.parse(defaults.fruit);
    defaultData.purchases.fruits[defaultFruitId.packageId] = {};
    defaultData.purchases.fruits[defaultFruitId.packageId][defaultFruitId.itemId] = true;
  } else {
    var datatext = middler.data.get('unsyncable');
    defaultData = {
      dark: false,
      sync: null,
      vanilla: false
    };
  }
  if (datatext) {
    return JSON.parse(datatext);
  } else {
    setUserData(defaultData, syncable);
    return defaultData;
  }
}

function setUserData(data, syncable) {
  var datatext = JSON.stringify(data);
  if (syncable) {
    middler.data.set('syncable', datatext);
  } else {
    middler.data.set('unsyncable', datatext);
  }
}

function findTranslatedText(obj) {
  if (typeof obj == 'string') {
    return obj;
  }
  var lang = middler.getLang();
  if (obj[lang]) {
    return obj[lang];
  } else if (obj.default) {
    return obj[obj.default];
  } else if (obj.en) {
    return obj.en;
  } else if (obj.it) {
    return obj.it;
  } else {
    return undefined;
  }
}

function sync(save, block, isSetup) {
  /* 
   * save .... if true upload data, if false download
   * block ... if true block the screen with a loading icon
   * isSetup . if true this is the first sync right after setup
  */
  var finish = function(deleteData) {
    if (deleteData) {
      var data = getUserData(false);
      data.sync = null;
      setUserData(data, false);
    } else {
      if (save) {
        toast(_('sync.postDone'));
      } else {
        toast(_('sync.getDone'));
      }
    }
    if (block) {
      loader(false);
    }
    if (isSetup && !deleteData) {
      if ($('#syncSetupCode').is(':visible')) {
        $('#syncSetupCode')[0].close();
      }
      if ($('#syncSetup').is(':visible')) {
        $('#syncSetup')[0].close();
      }
    }
  }
  var syncData = getUserData(false).sync;
  if (!syncData || !syncData.url || !syncData.token) {
    middler.dialog(_('error.title'), _('error.sync.syncDataCorrupted'), _('ok'), function() {});
    return finish(true);
  }
  if (block) {
    loader(true);
  }
  var saveData = {};
  var method = 'GET';
  if (save) {
    method = 'POST';
    saveData = getUserData(true);
  }
  $.ajax({
    url: syncData.url,
    method: 'GET',
    json: true,
    headers: ajaxHeaders(syncData.token),
    complete: function(jqXHR, _textStatus) {
      try {
        if (jqXHR.responseText == '' && (jqXHR.status >= 200 && jqXHR.status < 300)) {
          var infoData = {}
        } else {
          var infoData = JSON.parse(jqXHR.responseText);
        }
      } catch (ex) {
        middler.dialog(_('error.title'), _('error.sync.generic'), _('ok'), function() {});
        finish(false);
        throw ex;
      }
      if (infoData.version != version.sync) {
        middler.dialog(_('error.title'), _('error.sync.serverVersionNotSupported'), _('ok'), function() {});
        finish(false);
        throw 'unsupported gianniSync version: base supports ' + version.sync + ', server is ' + infoData.version;
      }
      $.ajax({
        url: infoData.dataEndpoint,
        method: method,
        json: true,
        headers: ajaxHeaders(syncData.token),
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(saveData),
        complete: function(jqXHR, _textStatus) {
          try {
            if (jqXHR.responseText == '' && (jqXHR.status >= 200 && jqXHR.status < 300)) {
              var data = false;
            } else {
              var data = JSON.parse(jqXHR.responseText);
            }
          } catch (ex) {
            middler.dialog(_('error.title'), _('error.sync.generic'), _('ok'), function() {});
            finish(false);
            throw ex;
          }
          if (data.error) {
            middler.dialog(_('error.title'), _('error.sync.server.' + data.error), _('ok'), function() {});
            return finish(data.error == 'invalid_token');
          }
          if (data === false) {
            finish(false);
          } else {
            setUserData(data, true);
            refresh();
            finish(false);
          }
        }
      });
    }
  });
}

function setupSync(setupData) {
  var goSetup = function(setupData) {
    loader(true);
    if (setupData.url && setupData.token) {
      $.ajax({
        url: setupData.url,
        method: 'GET',
        json: true,
        headers: ajaxHeaders(setupData.token),
        success: function(data) {
          if (
            data.version
            && data.name
            && data.description
            && data.homepage
            && data.icon
            && data.logout
            && data.delete
            && data.dataEndpoint
          ) {
            if (data.version != version.sync) {
              middler.dialog(_('error.title'), _('error.sync.serverVersionNotSupported'), _('ok'), function() {});
              loader(false);
              throw 'unsupported gianniSync version: base supports ' + version.sync + ', server is ' + data.version;
            }
            middler.dialogMultiple(_('sync.name'), _('sync.setup.postOrGet'), [_('cancel'), _('sync.post'), _('sync.get')], function(i) {
              if (i == 0) {
                return;
              }
              var data = getUserData(false);
              data.sync = setupData;
              setUserData(data, false);
              loader(true);
              setTimeout(function() { sync(i == 1, true, true); }, 100);
            });
            loader(false);
          } else {
            console.error('gianniSync setup server load error: invalid/missing info', data);
            middler.dialog(_('error.title'), _('error.sync.setupInvalidServer'), _('ok'), function() {});
            loader(false);
          }
        },
        error: function(a, b, c) {
          console.error('gianniSync setup server load error', a, b, c);
          middler.dialog(_('error.title'), _('error.sync.setupInvalidServer'), _('ok'), function() {});
          loader(false);
        }
      });
    } else {
      middler.dialog(_('error.title'), _('error.sync.invalidSetupCode'), _('ok'), function() {});
      loader(false);
    }
  }
  if (getUserData(false).sync) {
    middler.dialogMultiple(_('sync.name'), _('sync.setup.code.confirmOverwrite'), [_('no'), _('yes')], function(i) {
      if (i == 1) {
        goSetup(setupData);
      }
    })
  } else {
    goSetup(setupData);
  }
}

function ajaxHeaders(syncToken) {
  var headers = {
    'x-gianniapp-base-version': version.main,
    'x-gianniapp-middler-version': version.middler,
    'x-gianniapp-middler-name': middlerName,
    'x-giannisync-version': version.sync,
  };
  if (syncToken) {
    headers['x-giannisync-token'] = syncToken;
  }
  return headers;
}

function getSortedPackages() {
  var packagesSorted = [];
  for (var i in packages) {
    var pack = packages[i];
    pack.id = i;
    packagesSorted.push(pack);
  }
  packagesSorted.sort(function(a, b) {
    if (a.location == 'base' && b.location != 'base') {
      return -1;
    } else if (b.location == 'base' && a.location != 'base') {
      return 1;
    } else if (a.location == 'middler' && b.location != 'middler') {
      return -1;
    } else if (b.location == 'middler' && a.location != 'middler') {
      return 1;
    } else if (a.id.toUpperCase() > b.id.toUpperCase()) {
      return -1;
    } else if (a.id.toUpperCase() < b.id.toUpperCase()) {
      return 1;
    } else {
      return 0;
    }
  });
  return packagesSorted;
}

function reloadPackageList() {
  var packagesSorted = getSortedPackages();
  $('#packageList').html('');
  for (var pack of packagesSorted) {
    var tdImg = document.createElement('td');
    var img = document.createElement('img');
    $(img).attr('src', pack.res.icon.src);
    $(img).addClass('package-list-icon');
    $(tdImg).html('');
    $(tdImg).append(img);
    var tdText = document.createElement('td');
    var b = document.createElement('b');
    $(b).text(findTranslatedText(pack.name));
    var br = document.createElement('br');
    var i = document.createElement('i');
    $(i).text(findTranslatedText(pack.description));
    $(tdText).html('');
    $(tdText).append(b);
    $(tdText).append(br);
    $(tdText).append(i);
    var tr = document.createElement('tr');
    $(tr).attr('data-package', pack.id);
    $(tr).addClass('pointer');
    $(tr).on('click', function() {
      var packageId = $(this).attr('data-package');
      var pack = packages[packageId];
      $('#packageInfoDialog').attr('data-package', packageId);
      $('#packageName').text(findTranslatedText(pack.name));
      $('#packageDescription').text(findTranslatedText(pack.description));
      $('#packageInfoUninstall').attr('disabled', (pack.location == 'base' || pack.location == 'middler'));
      $('#packageInfoEmail').attr('disabled', !pack.author.email);
      $('#packageInfoEmail').attr('data-url', 'mailto:' + pack.author.email.toString());
      $('#packageInfoWebsite').attr('disabled', !pack.author.website);
      $('#packageInfoWebsite').attr('data-url', pack.author.website.toString());
      $('#packageInfoDialog')[0].showModal();
    });
    $(tr).append(tdImg);
    $(tr).append(tdText);
    $('#packageList').append(tr);
  }
}

function checkAchievementCondition(condition, data) {
  if (condition.type == 'level') {
    return (getLevel(data, {vanilla: condition.vanilla}) >= condition.target);
  } else if (condition.type == 'fruitcount') {
    var id = PackageManager.id.parse(condition.id);
    return (getFruit(id.packageId, id.itemId, data) >= condition.target);
  } else if (condition.type == 'vanillacount') {
    return (typeof data.vanillaFruits == 'number' && data.vanillaFruits >= condition.target);
  } else if (condition.type == 'purchase') {
    if (condition.purchaseType == 'fruit') {
      var id = PackageManager.id.parse(condition.id);
      return (data.purchases.fruits[id.packageId] && data.purchases.fruits[id.packageId][id.itemId]);
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function checkAchievement(achievement, data) {
  if (typeof data != 'object') data = getUserData(true);
  // loop through the condition sets.
  // if all the conditions inside one set are true, then the achievement is unlocked.
  for (var conditions of achievement.conditionSets) {
    var total = true;
    for (var condition of conditions) {
      total = total && checkAchievementCondition(condition, data);
      if (!total) break;
    }
    if (total) return true;
  }
  return false;
}

window.addEventListener('full-load', function() {
  // if the middler supports packages then load them, otherwise load only
  // default packages
  if (middler.features.packageManagement) {
    middler.packages.getList('middler', function(err, middlerPackages) {
      if (err) return middler.dialog('package loading error', err.toString(), 'OK', function() {});
      middler.packages.getList('main', function(err, mainPackages) {
        if (err) return middler.dialog('package loading error', err.toString(), 'OK', function() {});
        middler.packages.getList('managed', function(err, managedPackages) {
          if (err) return middler.dialog('package loading error', err.toString(), 'OK', function() {});
          for (var i of middlerPackages) {
            packagePreload.add('middler', i);
          }
          for (var i of mainPackages) {
            packagePreload.add('main', i);
          }
          for (var i of managedPackages) {
            packagePreload.add('managed', i);
          }
          for (var i of defaults.packages) {
            packagePreload.add('base', i);
          }
        });
      });
    });
  } else {
    for (var i of defaults.packages) {
      packagePreload.add('base', i);
    }
  }
})

window.addEventListener('packages-load', function() {
  i18next
  .use(i18nextXHRBackend)
  .init({
    lng: middler.getLang(),
    debug: false,
    resGetPath: baseUrl + "langs/__lng__.json",
    backend: {
      loadPath: baseUrl + "langs/{{lng}}.json"
    },
    fallbackLng: 'it',
    preload: knownLangs
  }, function(err, t) {
    if (err) {
      if (err.toString().startsWith('failed loading langs/') && err.toString().endsWith('.json')) {
        middler.setLang('it');
        window.dispatchEvent(new Event('lang-change'));
      } else {
        middler.dialog('i18next loading error', err.toString(), 'OK', function() {});
        throw(err);
      }
    }
    _ = t;
    jqueryI18next.init(i18next, $);

    $(window).on('resize', function() {
      if (Math.max(document.documentElement.clientWidth, window.innerWidth || 0) >= Math.max(document.documentElement.clientHeight, window.innerHeight || 0)) {
        $('.auto-width').removeClass('full-width');
        $('.auto-width').addClass('half-width');
        $('.auto-width-quarter').removeClass('half-width');
        $('.auto-width-quarter').addClass('quarter-width');
      } else {
        $('.auto-width').addClass('full-width');
        $('.auto-width').removeClass('half-width');
        $('.auto-width-quarter').addClass('half-width');
        $('.auto-width-quarter').removeClass('quarter-width');
      }
    });
    window.dispatchEvent(new Event('resize'));

    $('#toast').on('click', toast.next);

    $('#levelText').on('click', function() {
      if (!$('#main').is(':visible')) {
        $('#shop').slideUp();
        $('#freeDiv').slideUp();
        $('#main').slideDown();
      } else {
        var level = getLevel();
        var appleCount = getFruitsToNextLevel();
        $('#freeDiv').html('');
        var h1 = document.createElement('h1');
        h1.innerText = _('level.level', { level:level });
        h1.style.textAlign = 'center';
        $('#freeDiv').append(h1);
        var progressBarOuter = document.createElement('div');
        progressBarOuter.className = 'progress-bar-outer';
        var progressBarInner = document.createElement('div');
        progressBarInner.className = 'progress-bar-inner';
        progressBarInner.style.width = ((1 - (appleCount / getFruitsBetweenLevels())) * 100) + '%';
        $(progressBarOuter).append(progressBarInner);
        $('#freeDiv').append(progressBarOuter);
        var p = document.createElement('p');
        p.innerText = _('level.applesToNext', { count: appleCount });
        p.style.textAlign = 'center';
        $('#freeDiv').append(p);
        $('#freeDiv').append(document.createElement('hr'));
        var achievementHeader = document.createElement('h1');
        achievementHeader.innerText = _('achievements.title');
        achievementHeader.style.textAlign = 'center';
        $('#freeDiv').append(achievementHeader);
        var data = getUserData(true);
        for (var packageId in packages) {
          if (typeof packages[packageId].achievements == 'object') {
            if (!data.achievements) {
              data.achievements = {};
            }
            if (!data.achievements[packageId]) {
              data.achievements[packageId] = {};
            }
            for (var achievementId in packages[packageId].achievements) {
              var achievement = packages[packageId].achievements[achievementId];
              if (!data.achievements[packageId][achievementId]) {
                data.achievements[packageId][achievementId] = checkAchievement(achievement, data);
                setUserData(data, true);
              }
              var outerDiv = document.createElement('div');
              outerDiv.className = 'achievement-outer';
              var achievementName = document.createElement('p');
              achievementName.className = 'achievement-name';
              achievementName.innerText = findTranslatedText(achievement.name);
              var achievementDesc = document.createElement('p');
              achievementDesc.className = 'achievement-description';
              achievementDesc.innerText = findTranslatedText(achievement.description);
              if (data.achievements[packageId] && data.achievements[packageId][achievementId]) {
                outerDiv.className += ' achievement-unlocked';
              } else {
                outerDiv.className += ' achievement-locked';
                if (!achievement.visible) {
                  achievementName.innerText = _('achievements.hidden.name');
                  achievementDesc.innerText = _('achievements.hidden.description')
                }
              }
              $(outerDiv).append(achievementName);
              $(outerDiv).append(achievementDesc);
              $('#freeDiv').append(outerDiv);
            }
          }
        }
        $('#main').slideUp();
        $('#shop').slideUp();
        $('#freeDiv').slideDown();
      }
    });

    $('#fruit').on('click', function() {
      if (current.character.item) {
        if (current.itemState != 'using') {
          clearTimeout(timerTimeout);
          timerTimeout = setTimeout(function() {
            for (var i in current.character.item.reward) {
              if (i == 'coin') {
                addCoins(current.character.item.reward[i]);
              } else {
                var fruitId = PackageManager.id.parse(i);
                try {
                  addFruit(fruitId.packageId, fruitId.itemId, current.character.item.reward[i]);
                } catch (ex) {
                  console.log('error in adding reward fruits (package probably isn\'t loaded):', ex);
                }
              }
            }
            timerTimeout = setTimeout(function() {
              current.itemState = 'idle';
              refresh();
            }, 1500);
            current.itemState = 'done';
            refresh();
          }, current.character.item.duration);
          current.itemState = 'using';
          refresh();
        }
      } else {
        if (current.itemState < 3) {
          current.itemState++;
        } else {
          if (getUserData(false).vanilla) {
            var data = getUserData(true);
            data.vanillaFruits++;
            setUserData(data, true);
          } else {
            var currentFruitId = PackageManager.id.parse(current.fruit.id);
            addFruit(currentFruitId.packageId, currentFruitId.itemId, 1);
            addCoins(1);
          }
          current.itemState = 0;
        }
        refresh();
      }
    });

    $('#topFruitData').on('click', function() {
      if (!current.character.item) {
        if (getUserData(false).vanilla) {
          middler.dialog(_('error.title'), _('error.notAvailableInVanilla'), _('ok'), function() {});
          return;
        }
        var clickGen = function(packageId, fruitId) {
          return function() {
            changeFruit(packageId, fruitId);
          }
        }
        $('#freeDiv').html('');
        var data = getUserData(true);
        var generateFruitElement = function(packageId, fruitId) {
          var img = document.createElement('img');
          if (data.purchases.fruits[packageId] && data.purchases.fruits[packageId][fruitId]) {
            $(img).attr('src', packages[packageId].fruits[fruitId].res[0].src);
          } else {
            img.style.backgroundImage = 'url(' + packages[packageId].fruits[fruitId].res[0].src + ')';
            img.style.backgroundSize = 'contain';
            $(img).attr('src', baseUrl + 'img/indicators/no.png');
          }
          $(img).on('click', clickGen(packageId, fruitId));
          $(img).attr('title', findTranslatedText(packages[packageId].fruits[fruitId].name));
          $(img).attr('alt', findTranslatedText(packages[packageId].fruits[fruitId].name));
          $(img).addClass('quarter-width');
          $(img).addClass('pointer');
          var p = document.createElement('p');
          p.style.textAlign = 'center';
          $(p).append(img);
          return p;
        }
        var packagesSorted = getSortedPackages();
        for (var pack of packagesSorted) {
          if (!pack.fruits) continue;
          for (var fruitId in pack.fruits) {
            $('#freeDiv').append(generateFruitElement(pack.id, fruitId));
          }
        }
        refresh();
        $('#main').slideUp();
        $('#shop').slideUp();
        $('#freeDiv').slideDown();
      }
    });

    $('#coin').on('click', function() {
      if (getUserData(false).vanilla) {
        middler.dialog(_('error.title'), _('error.notAvailableInVanilla'), _('ok'), function() {});
        return;
      }
      if (getLevel() < 10) {
        middler.dialog(_('error.title'), _('error.coin.notLevel10'), _('ok'), function() {});
        return;
      }
      $('#shopCategories').html('');
      $('#shopCategories').show();
      $('#shopCategory').hide();
      $('#shopBack').hide();
      var clickGen = function(categoryId) {
        return function() {
          var clickGen2 = function(categoryId, packageId, itemId) {
            return function() {
              var data = getUserData(true);
              if (data.purchases[categoryId][packageId] && data.purchases[categoryId][packageId][itemId]) {
                return;
              }
              if (getCoins() < packages[packageId][categoryId][itemId].cost) {
                middler.dialog(_('error.title'), _('error.coin.notEnough', {
                  missingCoins: packages[packageId][categoryId][itemId].cost - getCoins()
                }), _('ok'), function() {});
                return;
              }
              data.coins -= packages[packageId][categoryId][itemId].cost;
              if (!data.purchases[categoryId][packageId]) {
                data.purchases[categoryId][packageId] = {};
              }
              data.purchases[categoryId][packageId][itemId] = true;
              setUserData(data, true);
              $('.shop-indicator[data-category=' + categoryId + '][data-package=' + packageId + '][data-item=' + itemId + ']').show();
              refresh();
            }
          };
          $('#shopCategory').html('');
          var data = getUserData(true);
          var packagesSorted = getSortedPackages();
          for (var pack of packagesSorted) {
            if (pack[categoryId]) {
              for (var itemId in pack[categoryId]) {
                if (categoryId == 'fruits' && defaults.fruit == PackageManager.id.strignify(pack.id, itemId)) continue;
                var item = pack[categoryId][itemId];
                var img = document.createElement('img');
                if (categoryId == 'fruits') {
                  $(img).attr('src', item.res[0].src);
                } else {
                  $(img).attr('src', item.res.image.src);
                }
                $(img).addClass('quarter-width');
                var span = document.createElement('span');
                $(span).text(findTranslatedText(item.name));
                var coinImg = document.createElement('img');
                $(coinImg).attr('src', baseUrl + 'img/coin.png');
                $(coinImg).attr('height', '10');
                var okImg = document.createElement('img');
                $(okImg).attr('src', baseUrl + 'img/indicators/ok.png');
                $(okImg).attr('height', '10');
                $(okImg).addClass('shop-indicator');
                $(okImg).attr('data-category', categoryId);
                $(okImg).attr('data-package', pack.id);
                $(okImg).attr('data-item', itemId);
                if (!data.purchases[categoryId][pack.id] || !data.purchases[categoryId][pack.id][itemId]) {
                  $(okImg).hide();
                }
                var p = document.createElement('p');
                $(p).append(okImg);
                $(p).append(' ');
                $(p).append(span);
                $(p).append(' ');
                $(p).append(coinImg);
                $(p).append(item.cost);
                var div = document.createElement('div');
                $(div).addClass('pointer');
                $(div).on('click', clickGen2(categoryId, pack.id, itemId));
                $(div).append(img);
                $(div).append(p);
                $('#shopCategory').append(div);
              }
            }
          }
          window.dispatchEvent(new Event('resize'));
          refresh();
          $('#shopCategories').slideUp();
          $('#shopCategory').slideDown();
          $('#shopBack').slideDown();
        }
      };
      for (var categoryId of shopCategories) {
        var a = document.createElement('a');
        $(a).attr('data-i18n', 'coin.categories.' + categoryId);
        $(a).attr('href', 'javascript:void(0)');
        $(a).on('click', clickGen(categoryId));
        var li = document.createElement('li');
        $(li).append(a);
        $('#shopCategories').append(li);
      }
      window.dispatchEvent(new Event('resize'));
      refresh();
      $('#main').slideUp();
      $('#freeDiv').slideUp();
      $('#shop').slideDown();
    });

    $('#shopExit').on('click', function() {
      $('#shop').slideUp();
      $('#main').slideDown();
    });

    $('#shopBack').on('click', function() {
      $('#shopCategory').slideUp();
      $('#shopBack').slideUp();
      $('#shopCategories').slideDown();
    });

    $('#character').on('click', function() {
      if (getUserData(false).vanilla) {
        middler.dialog(_('error.title'), _('error.notAvailableInVanilla'), _('ok'), function() {});
        return;
      }
      $('#freeDiv').html('');
      var clickGen = function(packageId, characterId) {
        return function() {
          changeCharacter(packageId, characterId);
        }
      }
      var packagesSorted = getSortedPackages();
      for (var pack of packagesSorted) {
        if (pack.characters) {
          for (var characterId in pack.characters) {
            character = pack.characters[characterId];
            var img = document.createElement('img');
            img.src = character.res.character.src;
            img.alt = findTranslatedText(character.name);
            img.title = findTranslatedText(character.name);
            $(img).addClass('pointer');
            $(img).addClass('auto-width-quarter');
            $(img).on('click', clickGen(pack.id, characterId));
            var p = document.createElement('p');
            p.style.textAlign = 'center';
            $(p).append(img);
            $('#freeDiv').append(p);
          }
        }
      }
      window.dispatchEvent(new Event('resize'));
      $('#main').slideUp();
      $('#shop').slideUp();
      $('#freeDiv').slideDown();
    });

    $('.open-data-url').on('click', function() {
      var url = $(this).attr('data-url');
      if ($(this).attr('data-url-attachinfo') == 'true') {
        var hash;
        if (url.includes('#')) {
          hash = url.substr(url.indexOf('#'));
          url = url.substr(0, url.indexOf('#'));
        }
        if (url.includes('?')) {
          url += '&';
        } else {
          url += '?';
        }
        url += 'gianniappBaseV=' + encodeURIComponent(version.main);
        url += '&gianniappLang=' + encodeURIComponent(middler.getLang());
        url += '&gianniappDark=' + encodeURIComponent((getUserData(false).dark).toString());
        url += '&gianniappMiddlerV=' + encodeURIComponent(version.middler);
        url += '&gianniappMiddler=' + encodeURIComponent(middlerName);
        url += '&giannisyncV=' + encodeURIComponent(version.sync);
        if (middler.features.syncRedirect && $(this).attr('data-url-attachredirect') == 'true') {
          url += '&giannisyncRedirect=' + encodeURIComponent(middler.getFeatureInfo('syncRedirect'));
        }
        if (hash) {
          url += hash;
        }
      }
      middler.url(url);
    });

    $('#syncGet').on('click', function() {
      sync(false, true, false);
    });

    $('#syncPost').on('click', function() {
      sync(true, true, false);
    });

    $('#syncLogout').on('click', function() {
      middler.dialogMultiple(_('sync.logout'), _('confirm'), [
        _('no'),
        _('yes')
      ], function(i) {
        if (i == 1) {
          loader(true);
          var syncData = getUserData(false).sync;
          $('#sync')[0].close();
          $.ajax({
            url: syncData.url,
            method: 'GET',
            json: true,
            headers: ajaxHeaders(syncData.token),
            complete: function(jqXHR, _textStatus) {
              try {
                if (!(jqXHR.responseText == '' && (jqXHR.status >= 200 && jqXHR.status < 300))) {
                  var infoData = JSON.parse(jqXHR.responseText);
                } else {
                  throw 'generic';
                }
              } catch (ex) {
                middler.dialog(_('error.title'), _('error.sync.generic'), _('ok'), function() {});
                loader(false);
                throw ex;
              }
              $.ajax({
                url: infoData.logout,
                method: 'POST',
                json: true,
                headers: ajaxHeaders(syncData.token),
                complete: function(jqXHR, _textStatus) {
                  try {
                    if (!(jqXHR.responseText == '' && (jqXHR.status >= 200 && jqXHR.status < 300))) {
                      JSON.parse(jqXHR.responseText);
                    }
                  } catch (ex) {
                    middler.dialog(_('error.title'), _('error.sync.generic'), _('ok'), function() {});
                    loader(false);
                    throw ex;
                  }
                  var data = getUserData(false);
                  data.sync = null;
                  setUserData(data, false);
                  loader(false);
                }
              });
            }
          });
        }
      });
    });

    $('#syncDelete').on('click', function() {
      var deleteAccount = ($(this).attr('data-i18n') == 'sync.delete.button.account');
      var title = _('sync.delete.button.data');
      if (deleteAccount) {
        title = _('sync.delete.button.account');
      }
      middler.dialogMultiple(title, _('confirm'), [
        _('no'),
        _('yes')
      ], function(i) {
        if (i == 1) {
          loader(true);
          var syncData = getUserData(false).sync;
          $('#sync')[0].close();
          $.ajax({
            url: syncData.url,
            method: 'POST',
            json: true,
            headers: ajaxHeaders(syncData.token),
            complete: function(jqXHR, _textStatus) {
              try {
                if (!(jqXHR.responseText == '' && (jqXHR.status >= 200 && jqXHR.status < 300))) {
                  var infoData = JSON.parse(jqXHR.responseText);
                } else {
                  throw 'generic';
                }
              } catch (ex) {
                middler.dialog(_('error.title'), _('error.sync.generic'), _('ok'), function() {});
                loader(false);
                throw ex;
              }
              $.ajax({
                url: infoData.delete,
                method: 'POST',
                json: true,
                headers: ajaxHeaders(syncData.token),
                complete: function(jqXHR, _textStatus) {
                  try {
                    if (!(jqXHR.responseText == '' && (jqXHR.status >= 200 && jqXHR.status < 300))) {
                      JSON.parse(jqXHR.responseText);
                    }
                  } catch (ex) {
                    middler.dialog(_('error.title'), _('error.sync.generic'), _('ok'), function() {});
                    loader(false);
                    throw ex;
                  }
                  var data = getUserData(false);
                  data.sync = null;
                  setUserData(data, false);
                  if (deleteAccount) {
                    middler.dialog(_('sync.delete.button.account'), _('sync.delete.finish.account'), _('ok'), function() {});
                    loader(false);
                  } else {
                    middler.dialogMultiple(_('sync.delete.button.data'), _('sync.delete.finish.data'), [_('ok'), _('sync.homepage')], function(i) {
                      if (i == 1) {
                        middler.url(infoData.homepage);
                      }
                    });
                    loader(false);
                  }
                }
              });
            }
          });
        }
      });
    });

    $('#syncBtn').on('click', function() {
      if (getUserData(false).sync) {
        var syncData = getUserData(false).sync;
        if (!syncData || !syncData.url || !syncData.token) {
          middler.dialog(_('error.title'), _('error.sync.syncDataCorrupted'), _('ok'), function() {});
          var data = getUserData(false);
          data.sync = null;
          return setUserData(data, false);
        }
        loader(true);
        $.ajax({
          url: syncData.url,
          method: 'GET',
          json: true,
          headers: ajaxHeaders(syncData.token),
          complete: function(jqXHR, _textStatus) {
            try {
              if (jqXHR.responseText == '' && (jqXHR.status >= 200 && jqXHR.status < 300)) {
                var infoData = {}
              } else {
                var infoData = JSON.parse(jqXHR.responseText);
              }
            } catch (ex) {
              middler.dialog(_('error.title'), _('error.sync.generic'), _('ok'), function() {});
              loader(false);
              throw ex;
            }
            if (infoData.version != version.sync) {
              middler.dialog(_('error.title'), _('error.sync.serverVersionNotSupported'), _('ok'), function() {});
              loader(false);
              throw 'unsupported gianniSync version: base supports ' + version.sync + ', server is ' + infoData.version;
            }
            $('#syncImage').attr('src', infoData.icon);
            $('#syncName').text(findTranslatedText(infoData.name));
            $('#syncDescription').text(findTranslatedText(infoData.description));
            $('#syncHomepage').attr('data-url', infoData.homepage);
            if (infoData.privacy) {
              $('#syncPrivacy').attr('data-url', infoData.privacy);
              $('#syncPrivacy').show();
            } else {
              $('#syncPrivacy').hide();
            }
            if (infoData.accountDeleteSupported) {
              $('#syncDelete').attr('data-i18n', 'sync.delete.button.account');
            } else {
              $('#syncDelete').attr('data-i18n', 'sync.delete.button.data');
            }
            refresh();
            $('#sync')[0].showModal();
            loader(false);
          }
        });
      } else {
        $('#syncFeatured').html('');
        $('#syncSetup')[0].showModal();
        loader(true);
        $.ajax({
          url: 'https://gianniapp.altervista.org/res/sync/featured.php',
          method: 'GET',
          json: true,
          headers: ajaxHeaders(),
          success: function(data) {
            var loadServer = function(url, tr) {
              $.ajax({
                url: url,
                method: 'GET',
                json: true,
                headers: ajaxHeaders(),
                success: function(data) {
                  if (
                    data.name
                    && data.description
                    && data.homepage
                    && data.icon
                    && data.login
                  ) {
                    var tdImg = $(tr).children()[0];
                    var img = document.createElement('img');
                    $(img).attr('src', data.icon);
                    $(img).addClass('sync-setup-server-image');
                    $(tdImg).html('');
                    $(tdImg).append(img);
                    var tdText = $(tr).children()[1];
                    var b = document.createElement('b');
                    $(b).text(findTranslatedText(data.name));
                    var br = document.createElement('br');
                    var i = document.createElement('i');
                    $(i).text(findTranslatedText(data.description));
                    $(tdText).html('');
                    $(tdText).append(b);
                    $(tdText).append(br);
                    $(tdText).append(i);
                    $(tr).attr('data-info', JSON.stringify(data));
                    $(tr).addClass('pointer');
                    $(tr).on('click', function() {
                      var data = JSON.parse($(this).attr('data-info'));
                      $('#syncSetupServerName').text(findTranslatedText(data.name));
                      $('#syncSetupServerDesc').text(findTranslatedText(data.description));
                      $('#syncSetupServerHomepage').attr('data-url', data.homepage);
                      if (data.privacy) {
                        $('#syncSetupServerPrivacy').attr('data-url', data.privacy);
                        $('#syncSetupServerPrivacy').show();
                      } else {
                        $('#syncSetupServerPrivacy').hide();
                      }
                      $('#syncSetupServerLogin').attr('data-url', data.login);
                      $('#syncSetupServerSignup').attr('data-url', data.signup);
                      $('#syncSetupServerSignup').attr('disabled', !data.signup);
                      $('#syncSetupServer')[0].showModal();
                    });
                  } else {
                    console.error('gianniSync featured server load error: invalid/missing info', data);
                  }
                },
                error: function(a, b, c) {
                  console.error('gianniSync featured server load error', a, b, c);
                }
              });
            }
            if ($.isArray(data)) {
              for (var url of data) {
                var img = document.createElement('img');
                $(img).attr('src', 'img/loading.gif');
                $(img).addClass('sync-setup-server-image');
                var tdImg = document.createElement('td');
                $(tdImg).append(img);
                var tdText = document.createElement('td');
                $(tdText).html(_('loading'));
                var tr = document.createElement('tr');
                $(tr).append(tdImg);
                $(tr).append(tdText);
                $('#syncFeatured').append(tr);
                loadServer(url, tr);
                loader(false);
              }
            } else {
              console.error('gianniSync featured server list load error: invalid list', data);
              var p = document.createElement('p');
              $(p).html(_('error.sync.featuredLoadErr'));
              $('#syncFeatured').append(p);
              loader(false);
            }
          },
          error: function(a, b, c) {
            console.error('gianniSync featured server list load error', a, b, c);
            var p = document.createElement('p');
            $(p).html(_('error.sync.featuredLoadErr'));
            $('#syncFeatured').append(p);
            loader(false);
          }
        });
      }
    });

    $('#syncSetupCodeOk').on('click', function() {
      var base64 = $('#syncSetupCodeBox').val();
      try {
        var jsonText = atob(base64);
      } catch (ex) {
        middler.dialog(_('error.title'), _('error.sync.invalidSetupCode'), _('ok'), function() {});
        throw ex;
      }
      try {
        var setupData = JSON.parse(jsonText);
      } catch (ex) {
        middler.dialog(_('error.title'), _('error.sync.invalidSetupCode'), _('ok'), function() {});
        throw ex;
      }
      setupSync(setupData);
    });

    $('#packageInfoUninstall').on('click', function() {
      var packageId = $('#packageInfoDialog').attr('data-package');
      middler.dialogMultiple(_('packages.manager.confirm.title'), _('packages.manager.confirm.uninstall', {
        packageName: findTranslatedText(packages[packageId].name)
      }), [ _('no'), _('yes') ], function(i) {
        if (i == 1) {
          PackageManager.uninstall(packageId, function(err) {
            if (err == 'cannot_uninstall') return;
            $('#packageInfoDialog')[0].close();
            reloadPackageList();
          });
        }
      })
    });

    $('#packageInstall').on('click', function() {
      loader(true);
      PackageManager.install(function(err, id) {
        loader(false);
        if (err == 'canceled') return;
        if (err == 'conflict') {
          if (packages[id].location == 'managed') {
            middler.dialog(_('error.title'), _('error.packages.conflict.managed', {
              packageId: id
            }), _('ok'), function() {});
          } else {
            middler.dialog(_('error.title'), _('error.packages.conflict.preinstalled', {
              packageId: id
            }), _('ok'), function() {});
          }
          return console.error('conflict');
        }
        if (err) {
          middler.dialog(_('error.title'), _('error.packages.decompressionError'), _('ok'), function() {});
          return console.error(err);
        }
        if (id) {
          PackageManager.loadWithDialog('main', id, function() {
            reloadPackageList();
          });
        }
      });
    });

    $('#settingsPackages').on('click', function() {
      reloadPackageList();
      $('#packageManager')[0].showModal();
    });

    if (!middler.features.packageManagement) {
      $('#settingsPackages').hide();
    }

    if (middler.features.externalDarkControl) {
      $('#settingsDark').attr('disabled', true);
      var data = getUserData(false);
      data.dark = middler.getFeatureInfo('externalDarkControl');
      setUserData(data, false);
    }

    $('#settingsDark')[0].checked = getUserData(data).dark;

    $('#settingsDark').on('change', function() {
      var data = getUserData(false);
      data.dark = $('#settingsDark')[0].checked;
      setUserData(data, false);
      refresh();
    });

    $('#settingsVanilla')[0].checked = getUserData(false).vanilla;

    $('#settingsVanilla').on('change', function() {
      var data = getUserData(false);
      data.vanilla = $('#settingsVanilla')[0].checked;
      setUserData(data, false);
      if ($('#settingsVanilla')[0].checked) {
        var defaultCharacterId = PackageManager.id.parse(defaults.character)
        var character = packages[defaultCharacterId.packageId].characters[defaultCharacterId.itemId];
        character.id = defaults.character;
        current.character = character;
        var defaultFruitId = PackageManager.id.parse(defaults.fruit)
        var fruit = packages[defaultFruitId.packageId].fruits[defaultFruitId.itemId];
        fruit.id = defaults.fruit;
        current.fruit = fruit;
        current.itemState = 0;
        $('#freeDiv').slideUp();
        $('#shop').slideUp();
        $('#main').slideDown();
      }
      refresh();
    });

    $('#settingsReset').on('click', function() {
      middler.dialogMultiple(_('settings.reset.reset'), _('settings.reset.confirm'), [
        _('no'),
        _('yes')
      ], function(i) {
        if (i == 1) {
          middler.data.delete(null);
          middler.quit();
        }
      });
    });

    $('#settingsAbout').on('click', function() {
      $('#about')[0].showModal();
    });

    for (var x in opensource) {
      var linkItem = document.createElement('a');
      $(linkItem).html(x);
      $(linkItem).attr('href', 'javascript:void(\'openSourceLicense\')')
      $(linkItem).on('click', function() {
        $('#openSourceName').html(this.innerText);
        $('#openSourceText').text(opensource[this.innerText]);
        $('#openSourceLicense')[0].showModal();
      });
      var listItem = document.createElement('li');
      $(listItem).append(linkItem);
      $('#openSourceList').append(listItem);
    }

    for (var i in knownLangs) {
      var code = knownLangs[i];
      var option = document.createElement('option');
      option.value = code;
      option.innerHTML = i18next.getFixedT(code)('language.name');
      $('#language').append(option);
    }
    $('#language').val(middler.getLang());

    $('#language').on('change', function() {
      middler.setLang($('#language').val());
      window.dispatchEvent(new Event('lang-change'));
    });

    var defaultCharacterId = PackageManager.id.parse(defaults.character)
    var character = packages[defaultCharacterId.packageId].characters[defaultCharacterId.itemId];
    character.id = defaults.character;
    current.character = character;
    var defaultFruitId = PackageManager.id.parse(defaults.fruit)
    var fruit = packages[defaultFruitId.packageId].fruits[defaultFruitId.itemId];
    fruit.id = defaults.fruit;
    current.fruit = fruit;
    current.itemState = 0;

    refresh();
    //autorefresh = setInterval(refresh, 1000);

    loader(false);
    $('#topBar').slideDown();
    $('#main').show();

    window.dispatchEvent(new Event('game-ready'));
  });
});

window.addEventListener('lang-change', function() {
  loader(true);
  lang = middler.getLang();
  if (lang == null) {
    lang = "it";
  }
  i18next.changeLanguage(lang, (err, t) => {
    if (err) {
      if (err.toString().startsWith('failed loading langs/') && err.toString().endsWith('.json')) {
        middler.setLang('it');
        window.dispatchEvent(new Event('lang-change'));
      } else {
        middler.dialog('i18next loading error', err.toString(), 'OK', function() {});
        throw(err);
      }
    }
    _ = t;
    refresh();
    loader(false);
  });
});
