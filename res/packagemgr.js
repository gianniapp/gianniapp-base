var packages = {}

var PackageManager = {
  support: {
    // support functions (event handler generators)
    resLoadComplete: function(id, getProgress) {
      return function() {
        packages[id].loading.current++;
        if (getProgress) {
          packages[id].loading.callback('progress');
        }
        if (packages[id].loading.current >= packages[id].loading.total) {
          packages[id].loading.callback(undefined);
          delete packages[id].loading;
        }
      }
    },
    resLoadError: function(id) {
      return function(e) {
        packages[id].loading.callback('resource_missing');
        delete packages[id];
        console.error('error while loading resources for package', id, e);
      }
    }
  },
  id: {
    regex: /^[a-z0-9-_]+$/g, // only ids that match this regex are valid
    strignify: function(packageId, itemId) {
      return packageId + '/' + itemId;
    },
    parse: function(stringId) {
      var parts = stringId.split('/');
      if (parts.length != 2) {
        throw 'invalid stringified id';
      }
      return {
        packageId: parts[0],
        itemId: parts[1]
      }
    }
  },
  getMeta: function(location, id, callback) {
    if (!id.match(PackageManager.id.regex)) {
      console.error('invalid package id');
      return callback('invalid_id', undefined);
    }
    if (location == 'middler' || location == 'main' || location == 'managed') {
      middler.packages.getMeta(location, id, function(err, data) {
        if (err) return callback(err, undefined);
        if (!data.name || !data.description || !data.author || !data.version) {
          console.error('invalid package meta');
          return callback('invalid', undefined);
        }
        return callback(undefined, data);
      });
    } else if (location == 'base') {
      $.ajax({
        url: baseUrl + 'packages/' + id + '/meta.json',
        method: 'GET',
        json: true,
        success: function(data) {
          if (typeof data == 'string') {
            try {
              data = JSON.parse(data);
            } catch (ex) {
              console.error('package meta retrieval error: invalid json');
              return callback('meta_retrieval_error', undefined);
            }
          }
          if (data.pkgVersion != version.package) {
            console.error('package version unsupported');
            return callback('version_unsupported', undefined);
          }
          if (!data.name || !data.description || !data.author || !data.version) {
            console.error('invalid package meta');
            return callback('invalid_meta', undefined);
          }
          return callback(undefined, data);
        },
        error: function(a, b, c) {
          console.error('package meta retrieval error:', a, b, c);
          return callback('meta_retrieval_error', undefined);
        }
      });
    } else {
      console.error('invalid package location');
      return callback('invalid_location', undefined);
    }
  },
  getResourceLocation(location, id, type, resourceName) {
    if (!id.match(PackageManager.id.regex) || !resourceName.match(PackageManager.id.regex)) {
      throw 'invalid package id or resource name';
    }
    if (location == 'middler' || location == 'main' || location == 'managed') {
      return middler.packages.getResourceLocation(location, id, type, resourceName);
    } else if (type.startsWith('fruit') && type.length == 'fruit'.length + 1) {
      var eatAmount = parseInt(type.substr('fruit'.length, 1));
      if (eatAmount >= 0 && eatAmount <= 3) {
        return baseUrl + 'packages/' + id + '/fruits/' + resourceName + '/' + eatAmount + '.png';
      } else {
        throw 'invalid resource type';
      }
    } else if (type == 'character') {
      return baseUrl + 'packages/' + id + '/characters/' + resourceName + '/character.png';
    } else if (type == 'characterItem') {
      return baseUrl + 'packages/' + id + '/characters/' + resourceName + '/item.png';
    } else if (type == 'characterItemDone') {
      return baseUrl + 'packages/' + id + '/characters/' + resourceName + '/item_done.png';
    } else if (type == 'characterItemUsing') {
      return baseUrl + 'packages/' + id + '/characters/' + resourceName + '/item_using.png';
    } else if (type == 'icon') {
      return baseUrl + 'packages/' + id + '/icon.png';
    } else {
      throw 'invalid resource type';
    }
  },
  loadWithDialog: function(location, id, callback) {
    $('#loadingDialogText').html(_('packages.loading.withoutName'));
    $('#loadingDialog')[0].showModal();
    PackageManager.load(location, id, true, function(err) {
      if (err == 'progress') {
        if (packages[id].loading && packages[id].loading.current && packages[id].loading.total) {
          $('#loadingDialogText').html(_('packages.loading.withCount', {
            packageName: findTranslatedText(packages[id].name),
            count: packages[id].loading.current,
            total: packages[id].loading.total
          }));
        } else {
          $('#loadingDialogText').html(_('packages.loading.withoutCount', {
            packageName: findTranslatedText(packages[id].name)
          }));
        }
      } else {
        $('#loadingDialog')[0].close();
        return callback(err);
      }
    });
  },
  load: function(location, id, getProgress, callback) {
    PackageManager.getMeta(location, id, function(err, data) {
      if (err) return callback(err.toString());
      if (packages[id]) {
        console.error('a package with id', id, 'has already been loaded or is being loaded');
        return callback('id_in_use');
      }
      packages[id] = data;
      packages[id].location = location;
      if (getProgress) {
        callback('progress');
      }
      packages[id].loading = {
        total: 0,
        current: 0,
        callback: callback
      };
      packages[id].loading.total++;
      var iconImg = new Image();
      $(iconImg).on('load', PackageManager.support.resLoadComplete(id, getProgress));
      $(iconImg).on('error', PackageManager.support.resLoadError(id));
      iconImg.src = PackageManager.getResourceLocation(location, id, 'icon', 'general');
      packages[id].res = {
        icon: iconImg
      };
      if (typeof data.fruits == 'object') {
        for (var fruitId in data.fruits) {
          packages[id].fruits[fruitId].res = {};
          for (var eatAmount = 0; eatAmount <= 3; eatAmount++) {
            packages[id].loading.total++;
            var img = new Image();
            $(img).on('load', PackageManager.support.resLoadComplete(id, getProgress));
            $(img).on('error', PackageManager.support.resLoadError(id));
            img.src = PackageManager.getResourceLocation(location, id, 'fruit' + eatAmount, fruitId);
            packages[id].fruits[fruitId].res[eatAmount] = img;
          }
        }
      }
      if (typeof data.characters == 'object') {
        for (var characterId in data.characters) {
          packages[id].characters[characterId].res = {};
          packages[id].loading.total++;
          var img = new Image();
          $(img).on('load', PackageManager.support.resLoadComplete(id));
          $(img).on('error', PackageManager.support.resLoadError(id));
          img.src = PackageManager.getResourceLocation(location, id, 'character', characterId);
          packages[id].characters[characterId].res.character = img;
          if (typeof data.characters[characterId].item == 'object' && data.characters[characterId].item != null) {
            var itemImg = new Image();
            $(itemImg).on('load', PackageManager.support.resLoadComplete(id));
            $(itemImg).on('error', PackageManager.support.resLoadError(id));
            itemImg.src = PackageManager.getResourceLocation(location, id, 'characterItem', characterId);
            packages[id].characters[characterId].res.item = itemImg;
            var itemDoneImg = new Image();
            $(itemDoneImg).on('load', PackageManager.support.resLoadComplete(id));
            $(itemDoneImg).on('error', PackageManager.support.resLoadError(id));
            itemDoneImg.src = PackageManager.getResourceLocation(location, id, 'characterItemDone', characterId);
            packages[id].characters[characterId].res.itemDone = itemDoneImg;
            var itemUsingImg = new Image();
            $(itemUsingImg).on('load', PackageManager.support.resLoadComplete(id));
            $(itemUsingImg).on('error', PackageManager.support.resLoadError(id));
            itemUsingImg.src = PackageManager.getResourceLocation(location, id, 'characterItemUsing', characterId);
            packages[id].characters[characterId].res.itemUsing = itemUsingImg;
            packages[id].loading.total += 3;
          }
        }
      }
      if (getProgress) {
        callback('progress');
      }
    });
  },
  unload: function(id, callback) {
    if (!packages[id]) return callback('not_loaded');
    if (defaults.packages.includes(id)) return callback('not_allowed');
    delete packages[id];
    callback(undefined);
  },
  install: function(callback) {
    return middler.packages.install(callback);
  },
  uninstall: function(id, callback) {
    var pack = packages[id];
    if (!pack) return;
    if (pack.location == 'base' || pack.location == 'middler') {
      callback('cannot_uninstall');
      return middler.dialog(_('error'), _('error.packages.cannotUninstall.preinstalled'), _('ok'), function() {});
    } else if (pack.location == 'managed') {
      callback('cannot_uninstall');
      return middler.dialog(_('error'), _('error.packages.cannotUninstall.managed'), _('ok'), function() {});
    }
    $('#loadingDialogText').html(_('packages.loading.uninstall', { packageName: findTranslatedText(pack.name) }));
    $('#loadingDialog')[0].showModal();
    PackageManager.unload(id, (err) => {
      if (err) {
        $('#loadingDialog')[0].close();
        return callback(err);
      }
      middler.packages.uninstall(id, pack.location, (err) => {
        $('#loadingDialog')[0].close();
        if (err) return callback(err.toString());
        return callback(undefined);
      });
    })
  }
};