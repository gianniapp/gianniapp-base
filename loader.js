'use strict';
var baseUrl = '';

var knownLangs = [
  'it', 'en', 'la'
];

var version = {
  main: '2.1.0',
  sync: 2,
  package: 2,
  middler: undefined
};

var middlerName = undefined;

var loadedScripts = {
  middler: false,
  openSource: false,
  fakeScript: false
}, loadedStyles = {
  fakeStyle: false
};
// the fakeScript and fakeStyle vars keep the game from finishing to load before
// the middler has said it's ready but allow it to use the loadScript and
// loadStyle functions to load any external scripts it may need

window.addEventListener('load', function() {
  var loadOpenSource = document.createElement("script");
  loadOpenSource.type = "text/javascript";
  loadOpenSource.src = baseUrl + 'res/opensource.js';
  loadOpenSource.addEventListener('load', scriptLoadOk('openSource'));
  loadOpenSource.addEventListener('load', function() {
    var loadMiddler = document.createElement("script");
    loadMiddler.type = "text/javascript";
    loadMiddler.src = baseUrl + 'middler.js';
    loadMiddler.addEventListener('load', scriptLoadOk('middler'));
    document.head.append(loadMiddler);
  });
  document.head.append(loadOpenSource);
});

var langs = [];

var loaded = {
  scripts: false,
  styles: false
}

var loader = function(x) {
  throw 'Not available';
}

loader.visible = false;

function allTrue(obj) {
  for(var o in obj) {
    if(!obj[o]) return false;
  }
  return true;
}

function loadOk(name) {
  return function() {
    loaded[name] = true;
    if (allTrue(loaded)) {
      $('dialog').each(function() {
        if (this.showModal === undefined) {
          dialogPolyfill.registerDialog(this);
        }
      });
      window.dispatchEvent(new Event('full-load'));
    }
  }
}

function scriptLoadOk(name) {
  return function() {
    loadedScripts[name] = true;
    if (name == 'jquery') {
      loadScript('jqueryLoading', 'lib/jquery-loading-overlay/2.1.6.min.js', '');
      loadScript('jqueryi18next', 'lib/jquery-i18next/1.2.1.min.js', '');
    }
    if (name == 'jqueryLoading') {
      loader = function(show) {
        if (show && !loader.visible) {
          loader.visible = true;
          for (var dialog of $('dialog')) {
            dialog.style.display = 'none';
          }
          $.LoadingOverlay('show', {
            image: baseUrl + 'img/loading.gif',
            fade: [0, 0],
            imageAnimation: false,
            imageColor: false,
            background: ($('body').hasClass('dark') ? 'rgba(50, 50, 50, 0.8)' : 'rgba(255, 255, 255, 0.8)')
          });
        } else if (!show && loader.visible) {
          for (var dialog of $('dialog')) {
            dialog.style.display = '';
          }
          $.LoadingOverlay('hide');
          loader.visible = false;
        }
      };
      loader(true);
      $('#pageLoading').hide();
    }
    if (allTrue(loadedScripts)) {
      var load = document.createElement("script");
      load.type = "text/javascript";
      load.src = baseUrl + "res/script.js";
      load.addEventListener('load', loadOk('scripts'));
      document.head.appendChild(load);
    }
  }
}

function styleLoadOk(name) {
  return function() {
    loadedStyles[name] = true;
    if (allTrue(loadedStyles)) {
      loadOk('styles')();
    }
  }
}

function loadScript(name, url) {
  loadedScripts[name] = false;
  var load = document.createElement("script");
  load.type = "text/javascript";
  load.src = url;
  load.addEventListener('load', scriptLoadOk(name));
  document.head.append(load);
}

function loadStyle(name, url) {
  loadedStyles[name] = false;
  var load = document.createElement("link");
  load.type = "text/css";
  load.rel = "stylesheet";
  load.href = url;
  load.addEventListener('load', styleLoadOk(name));
  document.head.append(load);
}


window.addEventListener('framework-ready', function() {
  loadedScripts.fakeScript = true;
  loadedStyles.fakeStyle = true;
  var knownStyles = [
    {
      'name': 'dialogPolyfill',
      'url': baseUrl + 'lib/dialog-polyfill/0.4.10.min.css'
    },
    {
      'name': 'fonts',
      'url': baseUrl + 'fonts/style.css'
    },
    {
      'name': 'main',
      'url': baseUrl + 'res/style.css'
    },
  ];

  var knownScripts = [
    {
      'name': 'jquery',
      'url': baseUrl + 'lib/jquery/3.3.1.min.js'
    },
    {
      'name': 'dialogPolyfill',
      'url': baseUrl + 'lib/dialog-polyfill/0.4.10.min.js'
    },
    {
      'name': 'i18next',
      'url': baseUrl + 'lib/i18next/12.0.0.min.js'
    },
    {
      'name': 'i18next-xhr-backend',
      'url': baseUrl + 'lib/i18next-xhr-backend/1.5.1.min.js'
    },
    {
      'name': 'package-manager',
      'url': baseUrl + 'res/packagemgr.js'
    }
  ]; // +main +jquery-loading-overlay +jquery-i18next +middler +opensource
  for (var i in knownScripts) {
    loadScript(knownScripts[i].name, knownScripts[i].url);
  }
  for (var i in knownStyles) {
    loadStyle(knownStyles[i].name, knownStyles[i].url);
  }
});
